templates
=========

A collection of free HTML templates

## Usage

Each template folder contains the core HTML template, as well as various static files in the *assets* folder.

Copy the template folder into your development environment and start working from there!

## List

The full list of available templates can be found here.

### minimal

* Minimalist design
* Grey/white colour scheme

![](assets/screenshots/minimalist/macbook_small.png)
![](assets/screenshots/minimalist/ipad_small.png)
![](assets/screenshots/minimalist/iphone_small.png)

### gallery *(coming soon)*

* Material design
* Blue/grey colour scheme
